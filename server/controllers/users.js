import User from '../models/user';


/**
* Preloads the user and atatches it to the req when the userid path is hit
*/
const load = (req, res, next, id) => {
	User.findById(id)
	.exec()
	.then((user) => {
		req.dbTask = user;

		return next();
	}, e => next(e));
};

/**
*	Returns previously assigned user object
*/
const get = (req, res) => {
	return res.json(req.dbTask);
};

/**
* Creates a new user (username an passowrd are mandatory)
*/
const create = (req, res, next) => {

	if (! req.body.username || ! req.body.password) {
		return res.json({
			'username': 'Unspecified username',
			'password': 'Unspecified password'
		})
	}

	// If no user found registered with the same email we can continue
	User.findOne({'email': req.body.email}, (err, user) => {
		if (! user) {
			return User.create({
				username: req.body.username,
				password: req.body.password,
				email: req.body.email
			})
			.then((savedUser) => {
				return res.json(savedUser);
			}, (e) => next(e));
		}

		res.json({
			err: 'User allready exists!'
		});

	})
};

/**
* Updates an user
*/
const update = (req, res, next) => {
	const user = req.dbTask;

	Object.assign(user, req.body);

	user.save()
		.then(() => res.sendStatus(204), e => next(e));
};

/**
* Lists a group of (limit param) users from req.query.skip as starting point
*/
const list = (req, res, next) => {
	const { limit = 50, skip = 0 } = req.query;

	User.find()
		.skip(skip)
		.limit(limit)
		.exec()
		.then((users) => res.json(users), e => next(e));
	};

/**
* Removes a user from the database
*/
const remove = (req, res, next) => {
	const user = req.dbTask;
	user.remove()
		.then(() => res.sendStatus(204), e => next(e));
};

export default {
	get,
	load,
	list,
	create,
	update,
	remove
};
