import User from '../models/user';


/**
*	Returns users friends-list
* The req.dbTask was previously assigend in users controller
*/
const get = (req, res) => {
	return res.json(req.dbTask.friendsList);
};

/**
* Creates a new friend request
*/
const create = (req, res, next) => {

	// Chec existing frined request
	const duplicateRequest = req.dbTask.friendRequests.sent.find(val => {
		return val.id === req.body.friendId;
	})

	if (! req.body.friendId) {
		return res.json({
			'status': 'Error',
			'info': 'Friend id required'
		});
	}

	if (duplicateRequest) {
		return res.json({
			'status': 'Error',
			'info': 'Friend request exists'
		});
	}

	// If no user found registered with the same email we can continue
	User.findById(req.body.friendId, (err, user) => {

		if (! user) {
				return res.json({
					'status': 'Error',
					'info': 'User not found'
				});
		}

		user.friendRequests.recieved.push({
			'id': req.dbTask.id,
			'name': req.dbTask.username,
			'created': Date.now()
		})
		user.save();

		req.dbTask.friendRequests.sent.push({
			'id': user.id,
			'name': user.username,
			'status': 'pending',
			'created': Date.now()
		})
		req.dbTask.save();

		res.json({
			'status': 'Success',
			'info': `Friend request sent to ${user.username}`
		});

	})
};
//
// /**
// * Updates an user
// */
// const update = (req, res, next) => {
// 	const user = req.dbTask;
//
// 	Object.assign(user, req.body);
//
// 	user.save()
// 		.then(() => res.sendStatus(204), e => next(e));
// };
//
// /**
// * Lists a group of (limit param) users from req.query.skip as starting point
// */
// const list = (req, res, next) => {
// 	const { limit = 50, skip = 0 } = req.query;
//
// 	User.find()
// 		.skip(skip)
// 		.limit(limit)
// 		.exec()
// 		.then((users) => res.json(users), e => next(e));
// 	};
//
// /**
// * Removes a user from the database
// */
// const remove = (req, res, next) => {
// 	const user = req.dbTask;
// 	user.remove()
// 		.then(() => res.sendStatus(204), e => next(e));
// };

export default {
	get,
	// load,
	// list,
	create
	// update,
	// remove
};
