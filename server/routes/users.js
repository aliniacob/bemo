import express from 'express';
import userCtrl from '../controllers/users';
import friendCtrl from '../controllers/friends';
import auth from '../../config/jwt';

const router = express.Router();

router.route('/')
	/** GET /api/users - Get list of users */
	.get(auth, userCtrl.list)

	/** POST /api/users - Create new user */
	.post(userCtrl.create);


router.route('/:userId')
	.all(auth)

	/** GET /api/users/:userId - Get user */
	.get(userCtrl.get)

	/** PUT /api/users/:userId - Update user */
	.put(userCtrl.update)

	/** DELETE /api/users/:userId - Delete user */
	.delete(userCtrl.remove);


router.route('/:userId/:friends')
	.all(auth)

	/** GET /api/users/:userId/:friends - Get friends list */
	.get(friendCtrl.get)

	/** POST /api/users/:userId/:friends - Send friend request */
	.post(friendCtrl.create)

	// /** PUT /api/users/:userId - Update user */
	// .put(userCtrl.update)
	//
	// /** DELETE /api/users/:userId - Delete user */
	// .delete(userCtrl.remove);


/** Load user when API with userId route parameter is hit */
router.param('userId', userCtrl.load);

export default router;
