export default {
  env: 'test',
  db: 'mongodb://localhost/node-es6-api-test',
  port: 3000,
  jwtSecret: 'asdasdasdsd',
  jwtDuration: '2 hours'
};
