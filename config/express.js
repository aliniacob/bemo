import express from 'express';
import expressValidation from 'express-validation';
import bodyParser from 'body-parser';
import routes from '../server/routes';
import path from 'path';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.resolve(__dirname, './../client')));
// mount all routes on /api path
app.use('/api', routes);
app.get("/", function (req, res) {
	res.sendFile(path.resolve(__dirname, './../client/index.html'))
});

app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    res.status(err.status).json(err);
  } else {
    res.status(err.status)
      .json({
        status: err.status,
        message: err.message
      });
  }
});


export default app;
