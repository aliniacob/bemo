import gulp from 'gulp';
import loadPlugins from 'gulp-load-plugins';
import path from 'path';
import del from 'del';
import runSequence from 'run-sequence';
import babelCompiler from 'babel-core/register';
import monkberrify from 'monkberrify';
import babelify from 'babelify';

// Load the gulp plugins into the `plugins` variable
const plugins = loadPlugins();

const paths = {
	js: ['./**/*.js', '!dist/**', '!gulpfile.babel.js', '!client/**', '!node_modules/**'],
  tests: './server/test/**/*.test.js'
};

// Compile all Babel Javascript into ES5 and put it into the dist dir
gulp.task('babel', () => {
  return gulp.src(paths.js, { base: '.' })
		.pipe(plugins.babel())
    .pipe(gulp.dest('dist'));
});

// Compile all Babel Javascript into ES5 and put it into the dist dir
gulp.task('html', () => {
  return gulp.src('./client/*.html')
    .pipe(gulp.dest('dist/client'));
});

gulp.task('sass', () => {
  return gulp.src('./client/css/*.scss')
		.pipe(plugins.sass())
    .pipe(gulp.dest('dist/client/css'));
});

// Compile all Babel Javascript into ES5 and put it into the dist dir
gulp.task('monk', () => {
  return gulp.src('./client/**/app.js')
		.pipe(plugins.browserify({
			options: {
				bundleOptions : {
					debug: true
				}
			},
			transform: ['monkberrify', 'babelify']
		}))
		.pipe(gulp.dest('dist/client'));
});

// Start server with restart on file change events
gulp.task('nodemon', ['babel', 'monk', 'html', 'sass'], () =>
  plugins.nodemon({
    script: path.join('dist', 'index.js'),
    ext: 'js',
    ignore: ['node_modules/**/*.js', 'dist/**/*.js', 'sass'],
    tasks: ['babel', 'monk', 'html', 'sass']
  })
);

// Clean up dist directory
gulp.task('clean', () => {
  return del('dist/**');
});

// Set environment variables
gulp.task('set-env', () => {
  plugins.env({
    vars: {
      NODE_ENV: 'test'
    }
  });
});

// triggers mocha tests
gulp.task('test', ['set-env'], () => {
  let exitCode = 0;

  return gulp.src([paths.tests], { read: false })
    .pipe(plugins.plumber())
    .pipe(plugins.mocha({
      reporter:'spec',
      ui: 'bdd',
      timeout: 2000,
      compilers: {
        js: babelCompiler
      }
    }))
    .once('error', (err) => {
      console.log(err);
      exitCode = 1;
    })
    .once('end', () => {
      process.exit(exitCode);
    });
});

gulp.task('mocha', ['clean'], () => {
  return runSequence('babel', 'test');
});
