import App from './../templates/App.monk';
export default class extends App {
  constructor() {
    super();

    // Define internal state of your component if you need to.
    this.state = {
      text: '',
      complete: false
    };

    this.on('click', '#main', this.handleClick.bind(this));
  }

	handleClick(e) {
		console.log(e);
	}


  update(state) {
    // Define actions to do on state updates.
    Object.assign(this.state, state);

    // Call update of view itself.
    super.update(this.state);
  }

}
