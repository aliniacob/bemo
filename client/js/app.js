import Monkberry from 'monkberry';
import 'monkberry-events';
import App from './models/App/views/App';


Monkberry.render(App, document.body);
